import json, warnings, errno, os, requests
import shutil, traceback, urllib, urllib3
import uuid, time, sys, re, getopt
from datetime import datetime

# get the arguments passed to the script
url = ''
project = ''
xitoken = ''
xiapp = ''

try:
   opts, args = getopt.getopt(sys.argv[1:],"h:u:p:t:a:",['url=', 'project=', 'token=', 'app='])
except getopt.GetoptError:
   print ('main.py -u <xiiot url to connect to> -p <project name within xiiot> -t <token>')
   sys.exit(2)
for opt, arg in opts:
   if opt == '-h':
      print ('main.py -u <xiiot url to connect to> -p <project name within xiiot> -t <token>')
      sys.exit()
   elif opt in ('-u', '--url'):
      url = arg 
   elif opt in ('-p', '--project'):
      project = arg
   elif opt in ('-t', '--token'):
      xitoken = arg
   elif opt in ('-a', '--app'):
      xiapp = arg
   else:
      print ('main.py -u <xiiot url to connect to> -p <project name within xiiot> -t <token>')
      sys.exit(2)

# include other python script so we can use their code
from iot_libs import projects
from iot_libs import node_info
from iot_libs import applications

# make sure there is no error code in the return from the API
# also make sure we got some result data returned
def check_api_return(returnm, result):
  if "statusCode" in returnm:
    print("API call failed with status code %s and error message:\n%s" % (returnm["statusCode"], returnm["message"]))
    sys.exit(1)
  # make sure there is data in the return body, only if result is set to one, otherwise we arent expecting a return body
  else:
    if result == 1:
      if "result" not in returnm:
        print("no result data in return")
        sys.exit(1) 

  return returnm

# get a list of all projects on the xiiot cluster
returnm = projects.get_projects(xitoken, url)
project_data = check_api_return(returnm,result=1)

# get project ID that matches our project name
for e in project_data["result"]:
  if project == e["name"]:
    project_id = e["id"]
try:
  project_id
except NameError:
  print("Couldnt get ID for %s, make sure the Project exists in Xi IoT" % project)
  sys.exit(1)

# create the list required to update/create an application in Xi IoT
local_appdata = {}

# get the EdgeIDs for that Project
#returnm = node_info.get_nodesinfo_byprojectid(xitoken, url, project_id)
#nodeinfo = check_api_return(returnm,result=1)

#local_appdata['edgeIds'] = []
#for e in nodeinfo['result']:
#  local_appdata['edgeIds'].append(e["id"])


# read the deployment config, so we have all the code stored that needs to be compared/updated
f = open("apps/%s/deployment.yml" % (xiapp), "r")
new_code = f.read()
file_byline = f.readlines()
f.close()

# go through the yaml line by line, so we can get the variable input from the header
# for example:
# description: this is a new app for xxxx, added another app
f = open("apps/%s/deployment.yml" % (xiapp), "r")
file_byline = f.readlines()
f.close()

local_appdata['appManifest'] = new_code
local_appdata['description'] = xiapp
for line in file_byline:
  line = line.rstrip()
  if re.match("#\sdescription\:\s.+", line):
    local_appdata['description'] = line.split(': ')[1]

# loop through remote apps to check if it already exists
returnm = applications.get_application_byprojectid(xitoken, url, project_id)
application_data = check_api_return(returnm,result=1)

match = 0
# loop through the remote applications for this project to see if it exists already
for remotea in application_data["result"]:
  if remotea["name"] == xiapp:
    match = 1
    app_id = remotea["id"]
    remote_code = remotea["appManifest"]

# create if it does not exist or do an update if it does exist and the Manifest has changed
if match == 0:
  print("%s currently does not exist in XiIoT, creating it for project %s" % (xiapp, project))
  returnm = applications.create_application(xitoken, url, local_appdata['appManifest'], local_appdata['description'], xiapp, project_id)
  check_api_return(returnm,result=0)
else:
  print("%s currently exists in XiIoT for project %s" % (xiapp, project))
  if (local_appdata['appManifest'] == remote_code):
    print("\nLocal and remote appManifest are the same, no action required. Most likely only the Docker Image changed.")
  else:
    print("\nUpdating App %s in XiIoT" % xiapp)
    app_update_data = applications.update_application(xitoken, url, app_id, local_appdata['appManifest'], local_appdata['description'])

    print(json.dumps(app_update_data))

    # scan the output to check if:
    # status code 200: update successful, means function is currently not used and can be updated
    # status code 400: function cant be updated as it is active in a pipeline, we need to create a clone and replace the existing function
    if "statusCode" not in app_update_data:
      print("\nApp Update successful")
    elif app_update_data["statusCode"] == 400:
      print("\nFunction Update failed with message: %s." % app_update_data["message"])
      sys.exit(1)

