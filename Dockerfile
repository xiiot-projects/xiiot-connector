FROM python:3.7-alpine

RUN pip3 install requests urllib3 uuid

ADD iot_libs iot_libs
COPY manage_app.py .
COPY manage_function.py .
COPY remote_cleanup.py .

#ENV MYVAR1 abc
