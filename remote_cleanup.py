import json, warnings, errno, os, requests
import shutil, traceback, urllib, urllib3
import uuid, time, sys, re, getopt
from datetime import datetime

# get the arguments passed to the script
url = ''
project = ''
xitoken = ''

try:
   opts, args = getopt.getopt(sys.argv[1:],"h:u:p:t:",['url=', 'project=', 'token='])
except getopt.GetoptError:
   print ('remote_cleanup.py -u <xiiot url to connect to> -p <project name within xiiot> -t <token>')
   sys.exit(2)
for opt, arg in opts:
   if opt == '-h':
      print ('remote_cleanup.py -u <xiiot url to connect to> -p <project name within xiiot> -t <token>')
      sys.exit()
   elif opt in ('-u', '--url'):
      url = arg 
   elif opt in ('-p', '--project'):
      project = arg
   elif opt in ('-t', '--token'):
      xitoken = arg
   else:
      print ('remote_cleanup.py -u <xiiot url to connect to> -p <project name within xiiot> -t <token>')
      sys.exit(2)

# include other python script so we can use their code
from iot_libs import functions 
from iot_libs import projects
from iot_libs import data_pipelines
from iot_libs import applications


# make sure there is no error code in the return from the API
# also make sure we got some result data returned
def check_api_return(returnm, result):
  if "statusCode" in returnm:
    print("API call failed with status code %s and error message:\n%s" % (returnm["statusCode"], returnm["message"]))
    sys.exit(1)
  # make sure there is data in the return body, only if result is set to one, otherwise we arent expecting a return body
  else:
    if result == 1:
      if "result" not in returnm:
        print("no result data in return")
        sys.exit(1) 

  return returnm


# get a list of all projects on the xiiot cluster
returnm = projects.get_projects(xitoken, url)
project_data = check_api_return(returnm,result=1)

# get project ID that matches our project name
for e in project_data["result"]:
  if project == e["name"]:
    project_id = e["id"]
try:
  project_id
except NameError:
  print("Couldnt get ID for %s, make sure the Project exists in Xi IoT" % project)
  sys.exit(1)

# cleanup all the functions that dont exist locally
print("\nStarting Cleanup of remote Functions that dont exist locally")

# make sure the functions folder exists
if not os.path.exists("functions"):
  print("functions directory does not exist within %s, skipping cleanup for functions." % project)
else:
  # get local functions
  local_functions = os.listdir("functions")

  # get a list of all functions within the project
  returnm = functions.get_function_byprojectid(xitoken, url, project_id)
  function_data = check_api_return(returnm,result=1)

  lfunctions_array = []
  rfunctions_array = []
  datestamp_list = {}

  if len(function_data["result"]) == 0:
    print("There currently are no old functions in %s, nothing to delete" % project)
    exit(0)

  for localf in local_functions:
    lfunctions_array.append(localf.split('.')[0])

  for remotef in function_data["result"]:
    name = re.sub(r'-\d{14}$', '', remotef["name"])

    # create a list of names, uniqu
    rfunctions_array.append(name)
    list_set = set(rfunctions_array)
    rfunctions_uniq = (list(list_set))

  # remove the functions that dont exist locally
  difference = list(set(rfunctions_uniq) - set(lfunctions_array))
  for function in difference:
    for f in function_data["result"]:
      if re.match(function+'(-\d{14}|)',f["name"]):
        print("Deleting %s as it does not exists in the local repo" % f["name"])
        returnm = functions.delete_function(xitoken, url, f["id"])
        if "statusCode" in returnm:
          print("Could not delete %s, error code %s with message:\n%s\n" % (f["name"], returnm["statusCode"], returnm["message"]))

  # get a list of clones, sort them and remove all functions that are older than the latest one
  for remotef in function_data["result"]:
    name = re.sub(r'-\d{14}$', '', remotef["name"])
    if name not in difference:
      datestamp = remotef["name"].partition("%s-" % name)[2]
      if name not in datestamp_list:
        datestamp_list[name] = []
      datestamp_list[name].append(datestamp)      

  for function in datestamp_list:
    datestamp_list[function].sort(reverse=True)
    counter = 1
    newest_function = ''
    newest_function_id = ''
    for datestamp in datestamp_list[function]:
      if counter == 1:
        counter += 1
        newest_function = function+'-'+datestamp
      else:
        for f in function_data["result"]:
          if f["name"] == function+'-'+datestamp:
            print("Deleting old clone %s %s with ID %s" % (function, datestamp, f["id"]))
            function_returnm = functions.delete_function(xitoken, url, f["id"])
            if "statusCode" not in function_returnm:
              # delete was successful
              print("Deleted %s with ID %s" % (f["name"], f["id"]))
            elif function_returnm["statusCode"] == 400:
              # find the function which we should be using
              for e in function_data["result"]:
                if e["name"] == newest_function:
                  newest_function_id = e["id"]
              if newest_function_id == '':
                print("Could not find ID for %s, cant fix IoT Pipeline, please check in GUI and make sure it uses the correct function." % newest_function)
              else:
                pipelines = re.search('other Data Pipelines\: (.+)$', function_returnm["message"]).group(1)
                print("\nFunction Delete failed as it is currently in use by %s, something went wrong in an earlier run during the update. The Pipeline should be usind the latest function %s with ID %s, but it is using %s" % (pipelines, newest_function, newest_function_id, f["name"]))

                pipeline_array = pipelines.split(', ')

                # get a list of all pipelines for that project
                returnm = data_pipelines.get_datapipelines_byprojectid(xitoken, url, project_id)
                pipelines = check_api_return(returnm,result=1)
  
                # for each pipeline the function belongs to perform an update to
                # replace the existing function with the new function ID
                for pipeline in pipeline_array:
                  for p in pipelines["result"]:
                    if pipeline == p["name"]:
                      pipeline_id = p["id"]

                  print("found pipeline %s with id %s" % (pipeline, pipeline_id))
           
                  # perform the function ID update
                  returnm = data_pipelines.update_datapipelines(xitoken, url, pipeline_id, newest_function_id, f["id"])
                  check_api_return(returnm,result=0)

            else:
              print("\nUnknown status code: %s, with message: %s" % (function_returnm["statusCode"],function_returnm["message"]))

  print("Done")

# cleanup all the applications that dont exist locally
print("\nStarting Cleanup of remote Applications that dont exist locally")

# make sure the apps folder exists
if not os.path.exists("apps"):
  print("apps directory does not exist within %s, not managing cleanup for apps." % project)
else:
  # get local apps
  local_apps = os.listdir("apps")

  # get a list of all functions within the project
  returnm = applications.get_application_byprojectid(xitoken, url, project_id)
  app_data = check_api_return(returnm,result=1)

  # loop remote apps and check if they match a local app
  for remotea in app_data["result"]:
    print("Processing remote app %s" % remotea["name"])
    match = 0

    # loop through local apps to see if we get a match
    for locala in local_apps:
      if remotea["name"] == locala:
        match = 1
   
    if match == 0:
      print("Didnt find a match for %s locally, deleting it." % remotea["name"])
      returnm = applications.delete_application(xitoken, url, remotea["id"])
      check_api_return(returnm,result=0)
    else:
      match = 0

  print("Done")
