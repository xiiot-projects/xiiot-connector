# xiiot-connector

API connector into Nutanix XiIoT, this Project also contains setup tools for a GitLab CI/CD integration

## Getting Started

The API connector requires an XiIoT account and an Authtoken which was generated in XiIoT.

The local directory structure needs to match the XiIoT setup and look like this:
```
<XiIoT Project name>
   |
   |--> functions
   |       |--> function1.py   (needs to match function name in XiIoT)
   |       |--> functionX.ext 
   |--> apps 
           |--> app1   (needs to match app name in XiIoT)
           |     |--> Dockerfile    (file to build the app container)
           |     |--> deployment.yml    (kubernetes deplyment yaml)
           |     |--> anotherfile.x   (any other files that are required as part of the container build)
           |--> appx
                 |--> Dockerfile
                 |--> deployment.yml
                 |--> anotherfile.y

```


The names need to match any pre existing functions or apps within XiIoT, otherwise the API connector will delete what does not exist locally. What is locally (or ideally in GitHub/Lab) is always seens as the source of thruth, anything that is created via the GUI will be deleted. You also need to copy the xiiot-connector python scripts (copy the lib directory + main python scripts) into the root of your Project if you want to run the script without the GitLab CICD integration.

There is a script that can be ran for the GitLab CICD integration, this will automatically create a .gitlab-ci.yml file in the root of your Project directory. This will trigger the API on a push automatically. It wont need the API connector Python scripts either as they are part of the python runner which is defined in the .gitlab-ci.yml. More further down on how to setup the GitLab CICD integration.

There is also a Dockerfile and docker-build.sh in the Project, this can be used to create/maintain your own python runner with the API connector.

!!! IMPORTANT !!! - You will have to build and push the app docker containers manually if you dont integrate into GitLab CICD, as those arent really part of the Nutanix API and a separate process.

## Requirements to integrate into GitLab CICD

You need a GitLab account to make use of their integrated Pipeline.
You need to create your own runners though as they need docker integration. This is fairly easy to do though, just install docker (docker.com) and follow this guide to setup a runner with docker integration:
https://angristan.xyz/2018/09/build-push-docker-images-gitlab-ci/

Make sure you setup the runner for the Group and not the Project, otherwise you will have to do it again everytime you add a new Project.
You can find the runner config here:
```
Groups -> Select the group you are using for XiIoT projects
   - Settings 
        |-> CI / CD
              |-> Runners
```
Also make sure to disable shared runners, they work for the python part of the pipeline but not for the docker build/push.

## Docker requirements for GitLab CICD

You need a docker account and add the credentials for DOCKER_USER and DOCKER_PASSWORD to the Group Variable in GitLab CICD.
The pipeline will the build and push containers automatically to your docker repository.
```
add your docker user to the Group as a variable, under the group (not project) menu:
   - Settings -> CI/CD -> Variables
   - Key DOCKER_USER: <your user>
   - Key DOCKER_PASSWORD: <your password>
```

## .gitlab-ci.yml - what does it do?

to do...

## Author

* ** Stefan Scharlott **
* ** Wolfgang Huse **

## Acknowledgements

 * Thanks to Stanislas Lange for his quick and easy guide to integrate docker into GitLab runners
 * Thanks to docker for well.. docker! :)
 * Thanks to the GitLab community for building and constantly enhancing their CICD
