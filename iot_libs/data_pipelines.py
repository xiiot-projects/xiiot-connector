import requests
import urllib3
import json

# remove some of the noise
requests.packages.urllib3.disable_warnings()
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# performs a simple get request against the Nutanix API to get a list of all the data pipelines
def get_datapipelines(xitoken, url):
  api_url = ("%s/v1.0/datapipelines" % (url))
  headers = { 'Authorization' : xitoken }

  r = requests.get(api_url, headers=headers, verify=False)
  j = json.loads(r.text)

  return j


# perform a get request against the Xi IoT API to the datapipelines that are in a specific project
def get_datapipelines_byprojectid(xitoken, url, projectid):
  api_url = ("%s/v1.0/projects/%s/datapipelines" % (url, projectid))
  headers = { 'Authorization' : xitoken }

  r = requests.get(api_url, headers=headers, verify=False)
  j = json.loads(r.text)

  return j


# perform a put request to update an existing datapipeline
# this methods only updates the function that matches with a new function ID (aka transformationId)
def update_datapipelines(xitoken, url, pipeline_id, new_function_id, function_id):
  api_url = ("%s/v1.0/datapipelines/%s" % (url, pipeline_id))
  headers = { 'Authorization' : xitoken }

  r = requests.get(api_url, headers=headers, verify=False)
  j = json.loads(r.text)

  match = 0
  for e in j["transformationArgsList"]:
    if e["transformationId"] == function_id:
      e["transformationId"] = new_function_id
      match = 1

  if match == 0:
    print("Error, could not find a match for %s in %s?!" % (function_id, j["name"]))
  else:
    print("Replaced %s with %s for API put" % (function_id, new_function_id))
    match = 0

  api_url = ("%s/v1.0/datapipelines/%s" % (url, pipeline_id))
  headers = { 'Authorization' : xitoken, 'Content-Type' : 'application/json' }

  r = requests.put(api_url, headers=headers, data=json.dumps(j), verify=False)
  j = json.loads(r.text)

  return j
