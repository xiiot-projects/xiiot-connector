import requests
import urllib3
import json

# remove some of the noise
requests.packages.urllib3.disable_warnings()
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# performs a simple get request against the Nutanix API to get a list of all the VMs
def get_functions(xitoken, url):
  api_url = ("%s/v1.0/functions" % (url))
  headers = { 'Authorization' : xitoken }

  r = requests.get(api_url, headers=headers, verify=False)
  j = json.loads(r.text)

  return j


def get_function_byprojectid(xitoken, url, projectid):
  api_url = ("%s/v1.0/projects/%s/functions" % (url, projectid))
  headers = { 'Authorization' : xitoken }

  r = requests.get(api_url, headers=headers, verify=False)
  j = json.loads(r.text)

  return j


# update a function in Xi IoT using the API and new parameters passed to the method
def update_function(xitoken, url, projectid, new_code, description, environment):
  api_url = ("%s/v1.0/functions/%s" % (url, projectid))
  headers = { 'Authorization' : xitoken }

  r = requests.get(api_url, headers=headers, verify=False)
  j = json.loads(r.text)

  j["code"] = new_code
  j["description"] = ("%s" % description)
  j["environment"] = environment

  print("new json to push:")
  print(json.dumps(j, indent=2)) 

  api_url = ("%s/v1.0/functions/%s" % (url, projectid))
  headers = { 'Authorization' : xitoken, 'Content-Type' : 'application/json' }

  r = requests.put(api_url, headers=headers, data=json.dumps(j), verify=False)
  j = json.loads(r.text)

  return j


# create a new function in Xi IoT using the API and parameters passed to the method
def create_function(xitoken, url, new_code, description, environment, language, name, projectid, runtime_id):
  api_url = ("%s/v1.0/functions" % url)
  headers = { 'Authorization' : xitoken, 'Content-Type' : 'application/json' }

  create_j = {
    "code": new_code,
    "description": description,
    "environment": environment,
    "language": language,
    "name": name,
    "params": [],
    "projectId": projectid,
    "type": "Transformation",
    "runtimeId": runtime_id
  }

  r = requests.post(api_url, headers=headers, data=json.dumps(create_j), verify=False)
  j = json.loads(r.text)

  return j


# delete function in Xi IoT based on the function ID
def delete_function(xitoken, url, function_id):
  api_url = ("%s/v1.0/functions/%s" % (url, function_id))
  headers = { 'Authorization' : xitoken }

  r = requests.delete(api_url, headers=headers, verify=False)
  j = json.loads(r.text)

  return j
