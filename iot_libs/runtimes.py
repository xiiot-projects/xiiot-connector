import requests
import urllib3
import json

# remove some of the noise
requests.packages.urllib3.disable_warnings()
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# performs a simple get request against the Nutanix API to get a list of all runtimes
def get_runtimes(xitoken, url):
  api_url = ("%s/v1.0/runtimeenvironments" % (url))
  headers = { 'Authorization' : xitoken }

  r = requests.get(api_url, headers=headers, verify=False)
  j = json.loads(r.text)

  return j


def get_runtime_byprojectid(xitoken, url, projectid):
  api_url = ("%s/v1.0/projects/%s/runtimeenvironments" % (url, projectid))
  headers = { 'Authorization' : xitoken }

  r = requests.get(api_url, headers=headers, verify=False)
  j = json.loads(r.text)

  return j
