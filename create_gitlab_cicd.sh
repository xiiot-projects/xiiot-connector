#!/bin/sh

#Display possible paramters and how to use them
helpFunction()
{
  echo ""
  echo "Usage: $0 -r repo -t token"
  echo -r "\trepository to be pushed to git"
  echo -t "\tXiIoT Authtoken (optional)"
  exit 1
}

#Check the arguemnts passed to the script
while getopts "r:t:" opt
do
  case "$opt" in
    r ) repo="$OPTARG" ;;
    t ) token="$OPTARG" ;;
    ? ) helpFunction ;;
  esac
done

#make sure the arguemnts arent empty
if [ -z "$repo" ]
then
  echo "repo parameter is empty...";
  helpFunction
fi

if [ -z "$token" ]
then
  echo "token paramter empty, setting it to <Xi IoT AuthToken>, this needs to be updated before the .gitlab-ci.yml is functional"
  token="<Xi IoT AuthToken>"
fi

#change to repo dir as the working directory, error if it doesnt exist
if [ -d "$repo" ]; then
  cd "$repo"
else
  echo "Directory $repo doesnt exist"
  exit 1
fi

echo "Working in `pwd`"

PROJECT_NAME=`pwd | awk -F/ '{print $NF}'` 

#create apps and functions dir if they dont exist
for DIR in apps functions
do
  mkdir -p $DIR
done


APP_STAGES=''
if [ "$(ls -A apps)" ]; then
  for i in `ls apps`; do
    APP_STAGES+='  - '$(printf '%s' "$i")$'\n  - xiiot_'$(printf '%s' "$i")$'_push\n'
APP_STAGE_DEFINITIONS+=$(printf '%s' "$i")$': 
  image: $DOCKER_RUNNER 
  variables: 
    APP_NAME: '$(printf '%s' "$i")$'
  stage: '$(printf '%s' "$i")$'
  script: 
  - docker info 
  - docker build -t $DOCKER_USER/'$(printf '%s' "$i")$':latest apps/'$(printf '%s' "$i")$'/ 
  - echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USER" --password-stdin 
  - docker push $DOCKER_USER/'$(printf '%s' "$i")$':latest 
  only: 
    changes: 
    - apps/'$(printf '%s' "$i")$'/Dockerfile
  allow_failure: true

xiiot_'$(printf '%s' "$i")$'_push:
  image: $PYTHON_RUNNER
  stage: xiiot_'$(printf '%s' "$i")$'_push
  script:
    - cp -f /*.py .
    - cp -af /iot_libs .
    - python3 manage_app.py -u $XIIOT_CLUSTER_URL -p "$XIIOT_PROJECT" -t "Bearer $XIIOT_AUTHTOKEN" -a "'$(printf '%s' "$i")$'"
  only:
    changes:
    - apps/'$(printf '%s' "$i")$'/*
  allow_failure: true

'

  done
else
  echo "apps dir is empty, not adding any app stages"
fi


cat > .gitlab-ci.yml << EOF
variables:
  XIIOT_CLUSTER_URL: https://iot.nutanix.com
  XIIOT_AUTHTOKEN: $token
  XIIOT_PROJECT: $PROJECT_NAME
  PYTHON_RUNNER: 'hornet83/xiiot-api:latest'
  DOCKER_RUNNER: 'docker:stable'

stages:
  - xiiot_function_push
$APP_STAGES  - xiiot_remote_cleanup

xiiot_function_push:
  image: \$PYTHON_RUNNER
  stage: xiiot_function_push
  script:
    - cp -f /*.py .
    - cp -af /iot_libs .
    - python3 manage_function.py -u \$XIIOT_CLUSTER_URL -p "\$XIIOT_PROJECT" -t "Bearer \$XIIOT_AUTHTOKEN"
  only:
    changes:
    - functions/*
  allow_failure: true

$APP_STAGE_DEFINITIONS
xiiot_remote_cleanup:
  image: \$PYTHON_RUNNER
  stage: xiiot_remote_cleanup
  script:
    - cp -f /*.py .
    - cp -af /iot_libs .
    - python3 remote_cleanup.py -u \$XIIOT_CLUSTER_URL -p "\$XIIOT_PROJECT" -t "Bearer \$XIIOT_AUTHTOKEN"
EOF

echo " ---------- completed ----------"
echo ""
echo "Make sure to open the .gitlab-ci.yml file and update the header variables to match your environment."
echo "XIIOT_CLUSTER_URL - if different to Nutanix default"
echo "XIIOT_AUTHTOKEN - this needs to be generated in the IoT portal"
echo "XIIOT_PROJECT - this should be correct if you named your git project folder the same as your IoT project"
echo "PYTHON_RUNNER - this needs to be the runner that contains the API scripts, make sure it uses the correct version as well!"
echo "DOCKER_RUNNER - the default should work, but in case it gets removed/changed, make sure this container can run docker commands"
echo ""
