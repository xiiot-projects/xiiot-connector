import json, warnings, errno, os, requests
import shutil, traceback, urllib, urllib3
import uuid, time, sys, re, getopt
from datetime import datetime

# get the arguments passed to the script
url = ''
project = ''
xitoken = ''

try:
   opts, args = getopt.getopt(sys.argv[1:],"h:u:p:t:",['url=', 'project=', 'token='])
except getopt.GetoptError:
   print ('main.py -u <xiiot url to connect to> -p <project name within xiiot> -t <token>')
   sys.exit(2)
for opt, arg in opts:
   if opt == '-h':
      print ('main.py -u <xiiot url to connect to> -p <project name within xiiot> -t <token>')
      sys.exit()
   elif opt in ('-u', '--url'):
      url = arg 
   elif opt in ('-p', '--project'):
      project = arg
   elif opt in ('-t', '--token'):
      xitoken = arg
   else:
      print ('main.py -u <xiiot url to connect to> -p <project name within xiiot> -t <token>')
      sys.exit(2)

# include other python script so we can use their code
from iot_libs import functions 
from iot_libs import projects
from iot_libs import data_pipelines
from iot_libs import runtimes


# make sure there is no error code in the return from the API
# also make sure we got some result data returned
def check_api_return(returnm, result):
  if "statusCode" in returnm:
    print("API call failed with status code %s and error message:\n%s" % (returnm["statusCode"], returnm["message"]))
    sys.exit(1)
  # make sure there is data in the return body, only if result is set to one, otherwise we arent expecting a return body
  else:
    if result == 1:
      if "result" not in returnm:
        print("no result data in return")
        sys.exit(1) 

  return returnm


# get a list of all projects on the xiiot cluster
returnm = projects.get_projects(xitoken, url)
project_data = check_api_return(returnm,result=1)

# get project ID that matches our project name
for e in project_data["result"]:
  if project == e["name"]:
    project_id = e["id"]
try:
  project_id
except NameError:
  print("Couldnt get ID for %s, make sure the Project exists in Xi IoT" % project)
  sys.exit(1)

# make sure the functions folder exists
if not os.path.exists("functions"):
  print("functions directory does not exist within %s, make sure it exists in your repository." % project)
  sys.exit(1)

# get local functions
local_functions = os.listdir("functions")

# get a list of all functions within the project
returnm = functions.get_function_byprojectid(xitoken, url, project_id)
function_data = check_api_return(returnm,result=1)

# loop through local functions
for localf in local_functions:
  if re.match('readme(.md|)', localf, flags=re.IGNORECASE):
    continue
  print("\nProcessing local script file %s" % localf)
  match = 0  

  # loop through remote functions to see if we get a match
  for remotef in function_data["result"]:
    name = re.sub(r'-\d{14}$', '', remotef["name"])
    language = remotef["language"]

    # confirm language and update the name with the extension
    # to match the local read filename
    if language == "python":
      scriptname=("%s.py" % name)
    elif language == "golang":
      scriptname=("%s.go" % name)
    elif language == "node":
      scriptname=("%s.js" % name)
    else:
      print("unknown language %s" % language)
      sys.exit(1)

    # if we ge a match update the variables we require for future methods
    if scriptname == localf:
      match = 1
      remote_code = remotef["code"]
      function_id = remotef["id"]
      remote_environment = remotef["environment"]
      remote_name = name
      remote_scriptname = scriptname

  # read the script into one file, so we have all the code stored that needs to be compared/updated
  local_functiondata = {}
  f = open("functions/%s" % (localf), "r")
  new_code = f.read()
  file_byline = f.readlines()
  f.close()

  # go through the script line by line, so we can get the variable input from the header
  # for example:
  # environment: python2-env
  # language: python
  # description: this is a new function for xxxx, added another function
  f = open("functions/%s" % (localf), "r")
  file_byline = f.readlines()
  f.close()

  local_functiondata['code'] = new_code
  local_functiondata['description'] = localf
  for line in file_byline:
    line = line.rstrip()
    if re.match("#\senvironment\:\s.+", line):
      local_functiondata['environment'] = line.split(': ')[1]
    if re.match("#\sdescription\:\s.+", line):
      local_functiondata['description'] = line.split(': ')[1]
    if re.match("#\slanguage\:\s.+", line):
      local_functiondata['language'] = line.split(': ')[1]

    # create a new unique filename using datetime
    name = localf.split('.')[0]
    now = datetime.now()
    dt_string = now.strftime("%Y%m%d%H%M%S")
    local_functiondata['name'] = ("%s-%s" % (name, dt_string))

  if "environment" not in local_functiondata:
    print("Couldnt get environment or runtime data from script file, make sure it is in the header!")
    sys.exit(1) 

  if "language" not in local_functiondata:
    print("Couldnt get language data from script file, make sure it is in the header!")
    sys.exit(1)

  # get the runtime ID and dockerRepoURI for update/create
  returnm = runtimes.get_runtime_byprojectid(xitoken, url, project_id)
  runtime_data = check_api_return(returnm,result=1)

  runtime_match = 0
  for runtime in runtime_data['result']:
    if (runtime['dockerRepoURI'] == local_functiondata['environment']):
      runtime_match = 1
      runtime_id = runtime['id']
      local_runtime_env = local_functiondata['environment']

  if runtime_match == 0:
    print("No runtime has the dockerRepoURI of %s in XiIoT, make sure it exists!" % local_functiondata['environment'])
    sys.exit(1)
  

  # create if it does not exist or do an update if it does exist
  if match == 0:
    print("%s currently does not exist in XiIoT, creating it for project %s" % (localf, project))
    returnm = functions.create_function(xitoken, url, local_functiondata['code'], local_functiondata['description'], local_runtime_env, local_functiondata['language'], local_functiondata['name'], project_id, runtime_id)
    check_api_return(returnm,result=0)
  else:
    print("%s currently exists in XiIoT for project %s" % (localf, project))
    if (local_functiondata['code'] == remote_code and local_functiondata['environment'] == remote_environment):
      print("\nLocal and remote function %s is the same, no action required." % remote_scriptname)
    else:
      print("\nUpdating function %s in XiIoT" % remote_scriptname)
      function_update_data = functions.update_function(xitoken, url, function_id, local_functiondata['code'], local_functiondata['description'], local_runtime_env)

      print(json.dumps(function_update_data))

      # scan the output to check if:
      # status code 200: update successful, means function is currently not used and can be updated
      # status code 400: function cant be updated as it is active in a pipeline, we need to create a clone and replace the existing function
      if "statusCode" not in function_update_data:
        print("\nFunction Update successful")
      elif function_update_data["statusCode"] == 400:
        pipelines = re.search('other Data Pipelines\: (.+)$', function_update_data["message"]).group(1)
        print("\nFunction Update failed as it is currently in use by %s, need to create it new as clone." % pipelines)

        pipeline_array = pipelines.split(', ')

        # create a new function with the same name + datetime stamp, this will be used further down
        # to replace the existion function in the pipeline
        returnm = functions.create_function(xitoken, url, local_functiondata['code'], local_functiondata['description'], local_functiondata['environment'], local_functiondata['language'], local_functiondata['name'], project_id)
        create_function_data = check_api_return(returnm,result=0)
        new_function_id = create_function_data["id"]

        # get a list of all pipelines for that project
        returnm = data_pipelines.get_datapipelines_byprojectid(xitoken, url, project_id)
        pipelines = check_api_return(returnm,result=1)

        # for each pipeline the function belongs to perform an update to
        # replace the existing function with the new function ID
        for pipeline in pipeline_array:
          for p in pipelines["result"]:
            if pipeline == p["name"]:
              pipeline_id = p["id"]
          
          print("found pipeline %s with id %s" % (pipeline, pipeline_id))
          
          # perform the function ID update
          returnm = data_pipelines.update_datapipelines(xitoken, url, pipeline_id, new_function_id, function_id)
          check_api_return(returnm,result=0)

      else:
        print("\nUnknown status code: %s, with message: %s" % (function_update_data["statusCode"],function_update_data["message"]))

  # reset match counter for next loop
  match = 0
